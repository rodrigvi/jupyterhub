# Author: Danilo Piparo, Enric Tejedor 2016
# Copyright CERN

"""CERN Spawn handler"""

import time
import os
from urllib.parse import parse_qs, unquote, urlparse
from .handlers_configs import SpawnHandlersConfigs
from socket import (
    gethostname,
)

from tornado import web
from tornado.httputil import url_concat

from jupyterhub.metrics import SERVER_POLL_DURATION_SECONDS
from jupyterhub.metrics import ServerPollStatus
from jupyterhub.utils import maybe_future
from jupyterhub.utils import url_path_join
from jupyterhub.handlers.base import BaseHandler

class SwanBaseHandler(BaseHandler):

    def handle_redirection(self, the_projurl = ''):
        ''' Return redirection url'''
        if not the_projurl:
            the_projurl = self.get_projurl()
        if not the_projurl: return ''

        return 'download?projurl=' + the_projurl

    def get_projurl(self):
        projurl = self.get_argument('projurl','')
        if not projurl:
            next_url = self.get_argument('next','')
            if next_url:
                unquoted_next = unquote(unquote(next_url))
                parsed_qs = parse_qs(urlparse(unquoted_next).query)
                if 'projurl' in parsed_qs:
                    projurl = parsed_qs['projurl'][0]
        return projurl

    def log_spawn_metrics(self, user, spawner, spawn_exception=None):
        """
        Log and send user chosen options. This will allow us to see what users are choosing.
        """
        host = gethostname().split('.')[0]

        ncores = spawner.get_user_cores()
        self.log_metric(user.name, host, "spawn_form_ncores", ncores)

        memory = spawner.get_user_memory()
        self.log_metric(user.name, host, "spawn_form_memory", memory)

        cluster = spawner.get_spark_cluster()
        self.log_metric(user.name, host, "spawn_form_spark_cluster", cluster)

        lcg_rel = spawner.get_lcg_release()
        lcg_rel_cleaned = lcg_rel.replace('/', '_')
        self.log_metric(user.name, host, "spawn_form_lcg_rel", lcg_rel_cleaned)

        platform = spawner.get_lcg_platform()
        platform_cleaned = platform.replace('/', '_')
        self.log_metric(user.name, host, "spawn_form_platform", platform_cleaned)

        if spawner.get_user_env_script_path():
            scriptenv = 1
        else:
            scriptenv = 0
        self.log_metric(user.name, host, "spawn_form_scriptenv", scriptenv)

        spawn_context_key = ".".join([spawner.get_lcg_release(), spawner.get_spark_cluster()])
        if spawn_exception:
            spawn_exc_class = spawn_exception.__class__.__name__
            self.log_metric(user.name, host, ".".join(["spawn_exception", spawn_context_key]), spawn_exc_class)
        else:
            self.log_metric(user.name, host, ".".join(["spawn_exception", spawn_context_key]), "None")

    def log_metric(self, user, host, metric, value):
        self.log.info("user: %s, host: %s, metric: %s, value: %s" % (user, host, metric, value))


class SpawnHandler(SwanBaseHandler):
    """Handle spawning of single-user servers via form.

    GET renders the form, POST handles form submission.

    Only enabled when Spawner.options_form is defined.
    """

    default_url = None

    async def _render_form(self, for_user, spawner_options_form, message=''):
        """
        Fork of https://github.com/jupyterhub/jupyterhub/blob/1.1.0/jupyterhub/handlers/pages.py#L86
        """

        auth_state = await for_user.get_auth_state()

        # SWAN customization required to handle projects from gallery
        the_projurl = self.get_projurl()
        the_form = open(spawner_options_form).read()
        if the_projurl:
            the_form +='<input type="hidden" name="projurl" value="%s">' % the_projurl

        return self.render_template(
            'spawn.html',
            for_user=for_user,
            auth_state=auth_state,
            spawner_options_form=the_form,
            error_message=message,
            url=self.request.uri,
            spawner=for_user.spawner,
        )

    @web.authenticated
    async def get(self, for_user=None, server_name=''):
        """GET renders form for spawning with user-specified options
        or triggers spawn via redirect if there is no form.

        Fork of https://github.com/jupyterhub/jupyterhub/blob/1.1.0/jupyterhub/handlers/pages.py#L86
        """

        # Get SpawnHandlersConfigs for SWAN
        configs = SpawnHandlersConfigs.instance()

        user = current_user = self.current_user
        if for_user is not None and for_user != user.name:
            if not user.admin:
                raise web.HTTPError(
                    403, "Only admins can spawn on behalf of other users"
                )

            user = self.find_user(for_user)
            if user is None:
                raise web.HTTPError(404, "No such user: %s" % for_user)

        if server_name:
            if not self.allow_named_servers:
                raise web.HTTPError(400, "Named servers are not enabled.")
            if (
                    self.named_server_limit_per_user > 0
                    and server_name not in user.orm_spawners
            ):
                named_spawners = list(user.all_spawners(include_default=False))
                if self.named_server_limit_per_user <= len(named_spawners):
                    raise web.HTTPError(
                        400,
                        "User {} already has the maximum of {} named servers."
                        "  One must be deleted before a new server can be created".format(
                            user.name, self.named_server_limit_per_user
                        ),
                    )

        if not self.allow_named_servers and user.running:
            url = self.get_next_url(user, default=user.server_url(server_name))
            self.log.info("User is running: %s", user.name)

            # SWAN customization to handle redirection considering projects
            redirect_url = self.handle_redirection()
            if redirect_url:
                url = os.path.join(url, redirect_url)
            else:
                url = os.path.join(url, configs.start_page)
            self.redirect(url)

            return

        if server_name is None:
            server_name = ''

        spawner = user.spawners[server_name]

        # SWAN customization required to show maintenance page
        if os.path.isfile(configs.maintenance_file):
            self.finish(self.render_template('maintenance.html'))
            return

        # SWAN customization required to show error massage on spawn fail
        if 'failed' in self.request.query_arguments:
            if spawner._spawn_future and spawner._spawn_future.exception():
                self.log_spawn_metrics(user, spawner, spawner._spawn_future.exception())

            spawner_options_form = await spawner.get_options_form()
            form = await self._render_form(
                for_user=user, spawner_options_form=spawner_options_form, message=configs.spawn_error_message)
            self.finish(form)
            return

        # resolve `?next=...`, falling back on the spawn-pending url
        # must not be /user/server for named servers,
        # which may get handled by the default server if they aren't ready yet

        pending_url = url_path_join(
            self.hub.base_url, "spawn-pending", user.escaped_name, server_name
        )

        if self.get_argument('next', None):
            # preserve `?next=...` through spawn-pending
            pending_url = url_concat(pending_url, {'next': self.get_argument('next')})

        # spawner is active, redirect back to get progress, etc.
        if spawner.ready:
            self.log.info("Server %s is already running", spawner._log_name)
            next_url = self.get_next_url(user, default=user.server_url(server_name))
            self.redirect(next_url)
            return

        elif spawner.active:
            self.log.info("Server %s is already active", spawner._log_name)
            self.redirect(pending_url)
            return

        # Add handler to spawner here so you can access query params in form rendering.
        spawner.handler = self

        # auth_state may be an input to options form,
        # so resolve the auth state hook here
        auth_state = await user.get_auth_state()
        await spawner.run_auth_state_hook(auth_state)

        spawner_options_form = await spawner.get_options_form()
        if spawner_options_form:
            self.log.debug("Serving options form for %s", spawner._log_name)
            form = await self._render_form(
                for_user=user, spawner_options_form=spawner_options_form, message='')
            self.finish(form)
        else:
            raise web.HTTPError(400, "Cannot spawn with default options")

    @web.authenticated
    async def post(self, for_user=None, server_name=''):
        """
        POST spawns with user-specified options

        Fork of https://github.com/jupyterhub/jupyterhub/blob/1.1.0/jupyterhub/handlers/pages.py#L86
        """

        # Get SpawnHandlersConfigs for SWAN
        configs = SpawnHandlersConfigs.instance()

        user = current_user = self.current_user
        if for_user is not None and for_user != user.name:
            if not user.admin:
                raise web.HTTPError(
                    403, "Only admins can spawn on behalf of other users"
                )
            user = self.find_user(for_user)
            if user is None:
                raise web.HTTPError(404, "No such user: %s" % for_user)

        spawner = user.spawners[server_name]

        if spawner.ready:
            raise web.HTTPError(400, "%s is already running" % (spawner._log_name))
        elif spawner.pending:
            raise web.HTTPError(
                400, "%s is pending %s" % (spawner._log_name, spawner.pending)
            )

        form_options = {}
        for key, byte_list in self.request.body_arguments.items():
            form_options[key] = [bs.decode('utf8') for bs in byte_list]
        for key, byte_list in self.request.files.items():
            form_options["%s_file" % key] = byte_list

        try:
            options = await maybe_future(spawner.options_from_form(form_options))
            await self.spawn_single_user(user, server_name=server_name, options=options)
        except Exception as e:
            self.log.error(
                "Failed to spawn single-user server with form", exc_info=True
            )

            # SWAN customization - catch proper exception on spawn start and log failed
            spawner_options_form = await spawner.get_options_form()
            if spawner._spawn_future and spawner._spawn_future.exception():
                spawn_exc = spawner._spawn_future.exception()
                spawn_error_message = str(spawn_exc)
            else:
                spawn_exc = e
                spawn_error_message = configs.spawn_error_message
            self.log_spawn_metrics(user, spawner, spawn_exc)
            form = await self._render_form(
                for_user=user, spawner_options_form=spawner_options_form, message=spawn_error_message)
            self.finish(form)

            return

        if current_user is user:
            self.set_login_cookie(user)
        next_url = self.get_next_url(
            user,
            default=url_path_join(
                self.hub.base_url, "spawn-pending", user.escaped_name, server_name
            ),
        )

        self.redirect(next_url)

class SpawnPendingHandler(SwanBaseHandler):
    """Handle /hub/spawn-pending/:user/:server
    One and only purpose:
    - wait for pending spawn
    - serve progress bar
    - redirect to /user/:name when ready
    - show error if spawn failed
    Functionality split out of /user/:name handler to
    have clearer behavior at the right time.
    Requests for this URL will never trigger any actions
    such as spawning new servers.
    """

    @web.authenticated
    async def get(self, for_user, server_name=''):
        """
        Fork of https://github.com/jupyterhub/jupyterhub/blob/1.1.0/jupyterhub/handlers/pages.py#L267
        """

        user = current_user = self.current_user
        if for_user is not None and for_user != current_user.name:
            if not current_user.admin:
                raise web.HTTPError(
                    403, "Only admins can spawn on behalf of other users"
                )
            user = self.find_user(for_user)
            if user is None:
                raise web.HTTPError(404, "No such user: %s" % for_user)

        if server_name and server_name not in user.spawners:
            raise web.HTTPError(
                404, "%s has no such server %s" % (user.name, server_name)
            )

        spawner = user.spawners[server_name]

        if spawner.ready:
            # spawner is ready and waiting. Redirect to it.
            self.log_spawn_metrics(user, spawner, None)
            next_url = self.get_next_url(default=user.server_url(server_name))
            self.redirect(next_url)
            return

        # if spawning fails for any reason, point users to /hub/home to retry
        self.extra_error_html = self.spawn_home_error

        auth_state = await user.get_auth_state()

        # First, check for previous failure.
        if (
            not spawner.active
            and spawner._spawn_future
            and spawner._spawn_future.done()
            and spawner._spawn_future.exception()
        ):
            # Condition: spawner not active and _spawn_future exists and contains an Exception
            # Implicit spawn on /user/:name is not allowed if the user's last spawn failed.
            # We should point the user to Home if the most recent spawn failed.
            exc = spawner._spawn_future.exception()
            self.log.error("Previous spawn for %s failed: %s", spawner._log_name, exc)

            # SWAN customization, do not redirect to not_running.html, go to spawn failed instead
            failed_url = url_path_join(
                self.hub.base_url, "spawn?failed"
            )
            self.redirect(failed_url)
            return

        # Check for pending events. This should usually be the case
        # when we are on this page.
        # page could be pending spawn *or* stop
        if spawner.pending:
            self.log.info("%s is pending %s", spawner._log_name, spawner.pending)
            # spawn has started, but not finished
            url_parts = []
            if spawner.pending == "stop":
                page = "stop_pending.html"
            else:
                page = "spawn_pending.html"
            html = self.render_template(
                page,
                user=user,
                spawner=spawner,
                progress_url=spawner._progress_url,
                auth_state=auth_state,
            )
            self.finish(html)
            return

        # spawn is supposedly ready, check on the status
        if spawner.ready:
            poll_start_time = time.perf_counter()
            status = await spawner.poll()
            SERVER_POLL_DURATION_SECONDS.labels(
                status=ServerPollStatus.from_status(status)
            ).observe(time.perf_counter() - poll_start_time)
        else:
            status = 0

        # server is not running, render "not running" page
        # further, set status to 404 because this is not
        # serving the expected page
        if status is not None:
            # SWAN customization, do not redirect to not_running.html, go to spawn changeconfig instead
            failed_url = url_path_join(
                self.hub.base_url, "spawn?changeconfig"
            )
            self.redirect(failed_url)
            return


        # we got here, server appears to be ready and running,
        # no longer pending.
        # redirect to the running server.
        self.log_spawn_metrics(user, spawner, None)
        next_url = self.get_next_url(default=user.server_url(server_name))
        self.redirect(next_url)
