# SWAN JupyterHub

- [CERNHandlers documentation](CERNHandlers/README.md)
- [SwanSpawner documentation](SwanSpawner/README.md)
- [start_jupyterhub.py bootstraping handlers and spawner](scripts/start_jupyterhub.py)
- [demo configuration](jupyterhub_config.py)