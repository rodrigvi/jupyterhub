from .swanspawner import define_SwanSpawner_from
from kubespawner import KubeSpawner

from tornado import gen

class SwanKubeSpawner(define_SwanSpawner_from(KubeSpawner)):

    @gen.coroutine
    def start(self):
        """
        Report start of the container
        @overwrites SwanSpawner.start()
        """

        self.cpu_limit = self.get_user_cores()
        self.mem_limit = self.get_user_memory()

        # start configured container
        startup = yield super().start()

        return startup