# Author: Danilo Piparo, Enric Tejedor, Diogo Castro 2015
# Copyright CERN

"""CERN Specific Spawner class"""

import re
import pwd
import time
from tornado import gen
from traitlets import (
    Unicode,
    Dict
)
from socket import (
    gethostname,
)


def define_SwanSpawner_from(base_class):
    """
        The Spawner need to inherit from a proper upstream Spawner (i.e Docker or Kube).
        But since our personalization, added on top of those, is exactly the same for all,
        by allowing a dynamic inheritance we can re-use the same code on all cases.
        This function returns our SwanSpawner, inheriting from a class (upstream Spawner)
        given as parameter.
    """

    class SwanSpawner(base_class):

        lcg_view_path = Unicode(
            default_value='/cvmfs/sft.cern.ch/lcg/views',
            config=True,
            help='Path where LCG views are stored in CVMFS.'
        )

        lcg_rel_field = Unicode(
            default_value='LCG-rel',
            help='LCG release field of the Spawner form.'
        )

        platform_field = Unicode(
            default_value='platform',
            help='Platform field of the Spawner form.'
        )

        user_script_env_field = Unicode(
            default_value='scriptenv',
            help='User environment script field of the Spawner form.'
        )

        user_n_cores_field = Unicode(
            default_value='ncores',
            help='User number of cores field of the Spawner form.'
        )

        user_memory_field = Unicode(
            default_value='memory',
            help='User available memory field of the Spawner form.'
        )

        spark_cluster_field = Unicode(
            default_value='spark-cluster',
            help='Spark cluster name field of the Spawner form.'
        )

        def __init__(self, **kwargs):
            super().__init__(**kwargs)

        @staticmethod
        def get_user_gid(username):
            return str(pwd.getpwnam(username).pw_gid)

        @staticmethod
        def get_user_id(username):
            return str(pwd.getpwnam(username).pw_uid)

        @staticmethod
        def get_hostname():
            return gethostname().split('.')[0]

        def get_lcg_view_path(self):
            return self.lcg_view_path

        def get_lcg_release(self):
            return self.user_options[self.lcg_rel_field]

        def get_lcg_platform(self):
            return self.user_options[self.platform_field]

        def get_user_env_script_path(self):
            return self.user_options[self.user_script_env_field]

        def get_user_cores(self):
            return self.user_options[self.user_n_cores_field]

        def get_user_memory(self):
            return self.user_options[self.user_memory_field]

        def get_spark_cluster(self):
            return self.user_options[self.spark_cluster_field]

        def options_from_form(self, formdata):
            """
            Get custom swan options for data

            @overwrites Spawner.options_from_form()
            """
            options = {}
            options[self.lcg_rel_field] = formdata[self.lcg_rel_field][0]
            options[self.platform_field] = formdata[self.platform_field][0]
            options[self.user_script_env_field] = formdata[self.user_script_env_field][0]
            options[self.spark_cluster_field] = formdata[self.spark_cluster_field][0] if self.spark_cluster_field in formdata.keys() else 'none'
            options[self.user_n_cores_field] = int(formdata[self.user_n_cores_field][0]) if self.user_n_cores_field in formdata.keys() else 1
            options[self.user_memory_field] = formdata[self.user_memory_field][0] + 'G' if self.user_memory_field in formdata.keys() else '2G'

            return options

        def get_env(self):
            """
            Set base environmental variables for swan systemuser image

            @overwrites Spawner.get_env()
            """
            env = super().get_env()

            username = self.user.name

            env.update(dict(
                # SWAN Notebook base env
                ROOT_LCG_VIEW_NAME=self.get_lcg_release(),
                ROOT_LCG_VIEW_PLATFORM=self.get_lcg_platform(),
                USER_ENV_SCRIPT=self.get_user_env_script_path(),
                ROOT_LCG_VIEW_PATH=self.get_lcg_view_path(),
                USER=username,
                USER_ID=self.get_user_id(username),
                USER_GID=self.get_user_gid(username),
                HOME= "/scratch/%s" % (username),
                SERVER_HOSTNAME="localhost",
                MAX_MEMORY=self.get_user_memory(),

                JPY_USER=self.user.name,
                JPY_COOKIE_NAME=self.user.server.cookie_name,
                JPY_BASE_URL=self.user.base_url,
                JPY_HUB_PREFIX=self.hub.base_url,
                JPY_HUB_API_URL=self.hub.api_url
            ))

            if self.get_spark_cluster() != 'none':
                env.update(dict(
                    # Spark base env
                    SPARK_USER=username,
                    SPARK_CLUSTER_NAME=self.get_spark_cluster(),
                    SPARK_PORTS="",
                    SPARK_CONFIG_SCRIPT="/srv/jupyterhub/spark-config.sh",
                ))

            return env

        @gen.coroutine
        def start(self):
            """
            Report start of the container
            @overwrites Spawner.start()
            """
            start_time_start_container = time.time()

            # start configured container
            startup = yield super().start()

            # log container start success metrics
            self.log_metric(
                self.user.name,
                self.get_hostname(),
                ".".join(["start_container_duration_sec", self.get_lcg_release(), self.get_spark_cluster()]),
                time.time() - start_time_start_container
            )

            return startup

        @gen.coroutine
        def stop(self, now=False):
            """
            Report stop of the container
            @overwrites Spawner.stop()
            """

            if self._spawn_future and not self._spawn_future.done():
                # Return 124 (timeout) exit code as container got stopped by jupyterhub before successful spawn
                container_exit_code = "124"
            else:
                # Return 0 exit code as container got stopped after spawning correctly
                container_exit_code = "0"

            stop_result = yield super().stop(now)

            self.log_metric(
                self.user.name,
                self.get_hostname(),
                ".".join(["exit_container_code"]),
                container_exit_code
            )

            return stop_result

        @gen.coroutine
        def poll(self):
            """
            Report get status of container
            @overwrites Spawner.poll()
            """
            container_exit_code = yield super().poll()

            # None if single - user process is running.
            # Integer exit code status, if it is not running and not stopped by JupyterHub.
            if container_exit_code is not None:
                exit_return_code = str(container_exit_code)
                if exit_return_code.isdigit():
                    value_cleaned = exit_return_code
                else:
                    result = re.search('ExitCode=(\d+)', exit_return_code)
                    if not result:
                        raise Exception("unknown exit code format for this Spawner")
                    value_cleaned = result.group(1)

                self.log_metric(
                    self.user.name,
                    self.get_hostname(),
                    ".".join(["exit_container_code"]),
                    value_cleaned
                )
            else:
                self.log_metric(
                    self.user.name,
                    self.get_hostname(),
                    ".".join(["running_container"]),
                    "1"
                )

            return container_exit_code

        def log_metric(self, user, host, metric, value):
            """
            Base metric logging to a log file. Spawners should override this method.
            """
            self.log.info("user: %s, host: %s, metric: %s, value: %s" % (user, host, metric, value))

    return SwanSpawner
