from .swanspawner import define_SwanSpawner_from
from dockerspawner import SystemUserSpawner

import os
from tornado import gen
from traitlets import (
    Int,
    Dict
)
import contextlib
import random
import psutil
from socket import (
    socket,
    SO_REUSEADDR,
    SOL_SOCKET,
)


class SwanDockerSpawner(define_SwanSpawner_from(SystemUserSpawner)):

    extra_env = Dict(
        config=True,
        help='Extra environment variables to pass to the container',
    )

    swan_session_num_ports = Int(
        default_value=3,
        config=True,
        help='Number of ports opened per user session (container).'
    )

    swan_port_range_start = Int(
        default_value=5001,
        config=True,
        help='Start of the port range that is used by the user session (container).'
    )

    swan_port_range_end = Int(
        default_value=5300,
        config=True,
        help='End of the port range that is used by the user session (container).'
    )

    shared_volumes = Dict(
        config=True,
        help='Volumes to be mounted with a "shared" tag. This allows mount propagation.',
    )

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def get_env(self):
        """
        Set base environmental variables
        """
        env = super().get_env()

        # overwrite default server hostname (requires public jupyterhub node hostname in docker-spawner mode)
        env['SERVER_HOSTNAME'] = os.uname().nodename

        if hasattr(self, 'extra_host_config') and hasattr(self, 'extra_create_kwargs'):
            # Clear old state
            self.extra_host_config['port_bindings'] = {}
            self.extra_create_kwargs['ports'] = []

            # Avoid overriding the default container output port, defined by the Spawner
            if not self.use_internal_ip:
                self.extra_host_config['port_bindings'][self.port] = (self.host_ip,)

        if self.extra_env:
            env.update(self.extra_env)

        return env


    @gen.coroutine
    def start(self):
        """Start the container and perform the operations necessary for mounting
        EOS, authenticating HDFS and authenticating K8S.
        @overwrites SwanSpawner.start()
        """

        cpu_quota = self.get_user_cores()
        mem_limit = self.get_user_memory()

        # The bahaviour changes if this if dockerspawner or kubespawner
        if hasattr(self, 'extra_host_config'):
            # Due to dockerpy limitations in the current version, we cannot use --cpu to limit cpu.
            # This is an alternative (and old) way of doing it
            self.extra_host_config.update({
                'cpu_period': 100000,
                'cpu_quota': 100000 * cpu_quota
            })
        self.mem_limit = mem_limit

        # start configured container
        startup = yield super().start()

        return startup

    @property
    def volume_mount_points(self):
        """
        Override this method to take into account the "shared" volumes.
        """
        return self.get_volumes(only_mount=True)

    @property
    def volume_binds(self):
        """
        Since EOS now uses autofs (mounts and unmounts mount points automatically), we have to mount
        eos in the container with the propagation option set to "shared".
        This means that: when users try to access /eos/projects, the endpoint will be mounted automatically,
        and made available in the container without the need to restart the session (it gets propagated).
        This also means that, if the user endpoint fails, when it gets back up it will be made available in
        the container without the need to restart the session.
        The Spawnwer/dockerpy do not support this option. But, if volume_bins return a list of string, they will
        pass the list forward until the container construction, without checking or trying to manipulate the list.
        """
        return self.get_volumes()

    def get_volumes(self, only_mount=False):

        def _fmt(v):
            return self.format_volume_name(v, self)

        def _convert_list(volumes, binds, mode="rw"):
            for k, v in volumes.items():
                m = mode
                if isinstance(v, dict):
                    if "mode" in v:
                        m = v["mode"]
                    v = v["bind"]

                if only_mount:
                    binds.append(_fmt(v))
                else:
                    binds.append("%s:%s:%s" % (_fmt(k), _fmt(v), m))
            return binds

        binds = _convert_list(self.volumes, [])
        binds = _convert_list(self.read_only_volumes, binds, mode="ro")
        return _convert_list(self.shared_volumes, binds, mode="shared")

    @staticmethod
    def get_reserved_port(start, end, n_tries=10):
        """
            Reserve a random available port.
            It puts the door in TIME_WAIT state so that no other process gets it when asking for a random port,
            but allows processes to bind to it, due to the SO_REUSEADDR flag.
            From https://github.com/Yelp/ephemeral-port-reserve
        """
        for i in range(n_tries):
            try:
                with contextlib.closing(socket()) as s:
                    port = random.randint(start, end)
                    net_connections = psutil.net_connections()
                    # look through the list of active connections to check if the port is being used or not and return FREE if port is unused
                    if next((conn.laddr[1] for conn in net_connections if conn.laddr[1] == port), 'FREE') != 'FREE':
                        raise Exception('Port {} is in use'.format(port))
                    s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
                    s.bind(('127.0.0.1', port))

                    # the connect below deadlocks on kernel >= 4.4.0 unless this arg is greater than zero
                    s.listen(1)

                    sockname = s.getsockname()

                    # these three are necessary just to get the port into a TIME_WAIT state
                    with contextlib.closing(socket()) as s2:
                        s2.connect(sockname)
                        s.accept()
                        return sockname[1]
            except:
                if i == n_tries - 1:
                    raise
