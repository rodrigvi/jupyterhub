# SWAN Spawner

Obtains credentials for the user and spawns a container for her.

## Installation

First, install dependencies:

    pip3 install .

## Usage

Add to your JupyterHub config file

    c.JupyterHub.spawner_class = 'swanspawner.SwanDockerSpawner'

If you deploy with Docker, or

    c.JupyterHub.spawner_class = 'swanspawner.SwanKubeSpawner'

If you deploy with Kubernetes.

## SwanSpawner - Jupyter Notebook environment variables

| env      |
|  ---     | 
| ROOT_LCG_VIEW_NAME   |
| ROOT_LCG_VIEW_PLATFORM   |
| USER_ENV_SCRIPT   |
| ROOT_LCG_VIEW_PATH   |
| USER  |
| USER_ID  |
| USER_GID  |
| HOME  |
| SERVER_HOSTNAME  |
| MAX_MEMORY  |
| JPY_USER  |
| JPY_COOKIE_NAME  |
| JPY_BASE_URL  |
| JPY_HUB_PREFIX  |
| JPY_HUB_API_URL  |
| SPARK_USER  |
| SPARK_CLUSTER_NAME  |
| SPARK_PORTS  |
| SPARK_CONFIG_SCRIPT  |